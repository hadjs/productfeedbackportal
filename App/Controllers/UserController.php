<?php

namespace App\Controllers;

use App\Database\Database;
use App\Models\User;
use PDO;

class UserController extends Controller
{
    public function login() {
        // Démarre la session
        session_start();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Récupérer les données du formulaire de connexion
            $username = $_POST['username'];
            $password = $_POST['password'];

            // Récupérer l'utilisateur en fonction du nom d'utilisateur (username)
            $db = Database::getInstance()->getConnection();
            $userModel = new User($db);
            $user = $userModel->getUserByUsername($username);

            // Vérifier le mot de passe et authentifier l'utilisateur
            if ($user && password_verify($password, $user->getPassword())) {
                $_SESSION['id'] = $user->getId();
                // Rediriger l'utilisateur vers la page addComment
                $this->renderView('comments/addComment', ['clientId' => $user->getId()]);

                $_SESSION['message'] = "Connexion réussie ! Bienvenue, " . $username . " !";
                exit();
            } else {
                $this->renderView('auth/login', []);
                // Afficher un message d'erreur sur la page de connexion
                $_SESSION['error'] = "Identifiant ou mot de passe incorrect. Veuillez réessayer.";
                exit();
            }
        } else {
            $this->renderView('auth/login', []);
        }
    }

    public function logout() {
        session_start();
        // Détruire toutes les données de session
        session_destroy();
        // Rediriger l'utilisateur vers la page de connexion ou une autre page appropriée après la déconnexion
        $this->renderView('auth/login', []);
        exit();
    }
}
