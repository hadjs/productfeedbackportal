<?
namespace App\Controllers;

class Controller
{
    protected $viewPath;

    public function __construct()
    {
        $this->viewPath = realpath(__DIR__ . '/../Views/') . '/';
      
    }

    protected function renderView($view, $data = [])
    {
        ob_start();
        $view = $view[0] === '/' ? substr($view, 1) : $view;     
        if ($data) {
            extract($data);
        }    
        // Utilisez la constante __DIR__ pour obtenir le répertoire actuel du fichier Controller.php
        $viewFilePath = __DIR__ . '/../Views/' . $view . '.php';
        if (file_exists($viewFilePath)) {
            require $viewFilePath;
        } else {
            echo 'Vue introuvable';
        }
    
        $content = ob_get_clean();
    
        require $this->viewPath . 'layout.php';
    }

   
}
