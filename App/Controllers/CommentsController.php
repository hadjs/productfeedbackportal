<?
namespace App\Controllers;

use App\Database\Database;
use App\Models\Comments;
use App\Models\User;
use PDO;

class CommentsController extends Controller
{
    public function index($vars)
    {
        // Extract variables from the $vars array
        $page = (int)($vars['page'] ?? 1); // Cast the 'page' parameter to an integer and default to 1 if it's not provided
        $commentsPerPage = 2;
    
        // Fetch comments for the specified page and comments per page
        $comments = $this->getCommentsForPage($page, $commentsPerPage);
    
        // Calculate total comments for pagination
        $totalComments = $this->getTotalComments();       
        $totalPages = ceil($totalComments / $commentsPerPage);
       
        $paginationData = [
            'currentPage' => $page,
            'totalPages' => $totalPages,
            'commentsPerPage' => $commentsPerPage,
        ];
    
        $this->renderView('comments/index', ['comments' => $comments, 'paginationData' => $paginationData]);
    }
    
    private function getCommentsForPage($page, $commentsPerPage)
    {
        // Calculate the offset to fetch comments for the current page
        $offset = ($page - 1) * $commentsPerPage;
    
        // Replace this with your actual code to fetch comments using the appropriate model method
        $commentsModel = new Comments(null, null, null, null, null, Database::getInstance()->getConnection());
        $comments = $commentsModel->fetchAllByQuery(
            "SELECT * FROM comments LIMIT :limit OFFSET :offset",
            ['limit' => $commentsPerPage, 'offset' => $offset]
        );
    
        return $comments;
    }
    
    private function getTotalComments()
    {
        // Replace this with your actual code to fetch the total count of comments using the appropriate model method
        $commentsModel = new Comments(null, null, null, null, null, Database::getInstance()->getConnection());
        $totalComments = $commentsModel->fetchCountByQuery("SELECT COUNT(*) as total FROM comments");
        return $totalComments; 
    }
    

    public function showCommentsByproduitId($produitId)
    {
        $commentsModel = new Comments(null, null, null, null, null, Database::getInstance()->getConnection());
        $produitId = (int)$produitId["produitId"];       
        $comments = $commentsModel->fetchAllByQuery("SELECT * FROM comments WHERE produitId = :produitId", ['produitId' => $produitId]);
        $this->renderView('comments/index', ['comments' => $comments]);
    }


    public function addComment()
    {
        $db = Database::getInstance()->getConnection(); 
          $userModel = new User($db);
         
        if ($userModel->isClientLoggedIn()) {
            $this->renderView('comments/addComment', []);
           
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
       
            $pdo = Database::getInstance()->getConnection();
            $clientId = (int)$_POST['clientId'];
            $produitId = $_POST['produitId'];
            $contenu = $_POST['contenu'];
            $newComment = new Comments(null, $produitId, $clientId, $contenu, date('Y-m-d H:i:s'), $pdo);

            try {
                $newComment->save();
                echo "Comment added successfully!";                
                } catch (Exception $e) {
                echo "Failed to add comment: " . $e->getMessage();
            }
        }
    }
   }

}
