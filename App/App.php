<?php

namespace App;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Classe App pour gérer les requêtes HTTP et retourner des réponses.
 *
 * @category  Application
 * @package   App
 * @license   https://www.example.com/license
 * @link      https://www.example.com
 */
class App
{

    private $modules = [];

    /*
    * App constructor
    * @param string[] $modules liste de modules à charger 
    */
    public function __construct(array $modules)
    {
        foreach ($modules as $module)
        {
          $this->modules[] = new $module();
        }
        
    }


    /**
     * Traite la requête et retourne une réponse HTTP appropriée.
     *
     * @param ServerRequestInterface $request L'objet représentant la requête HTTP.
     *
     * @return ResponseInterface L'objet représentant la réponse HTTP.
     */
    public function run(ServerRequestInterface $request): ResponseInterface
    {
        $uri = $request->getUri()->getPath();

        if (!empty($uri) && $uri[-1] === '/') {
            return (new Response())
                ->withStatus(301)
                ->withHeader('Location', substr($uri, 0, -1));
        }

        if ($uri === '/Commentaire') {
            return new Response(200, [], '<h1>Bienvenue sur le blog</h1>');
        }

        return new Response(404, [], '<h1>Erreur 404</h1>');
    }
}
