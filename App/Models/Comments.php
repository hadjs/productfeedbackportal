<?php

namespace App\Models;

use App\Models\Model;
use PDO;

class Comments extends Model
{ protected $table = 'comments';

    
    private $id;
    private $produitId;
    private $clientId;
    private $contenu;
    private $date;

    // Constructeur
    public function __construct($id, $produitId, $clientId, $contenu, $date, PDO $db)
    {
        parent::__construct($db); 

        $this->id = $id;
        $this->produitId = $produitId;
        $this->clientId = $clientId;
        $this->contenu = $contenu;
        $this->date = $date;
    }

    // Getters et Setters
    public function getId()
    {
        return $this->id;
    }

    public function getProduitId()
    {
        return $this->produitId;
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function getContenu()
    {
        return $this->contenu;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setProduitId($produitId)
    {
        $this->produitId = $produitId;
    }

    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function save()
    {
        if ($this->table === null) {
            throw new Exception("Table name is not specified.");
        }

        $properties = get_object_vars($this);
        unset($properties['table']);
        unset($properties['db']);

        $columns = implode(', ', array_keys($properties));
        $placeholders = ':' . implode(', :', array_keys($properties));

        $sql = "INSERT INTO {$this->table} ({$columns}) VALUES ({$placeholders})";

        $stmt = $this->db->prepare($sql);

        foreach ($properties as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Failed to save data: " . $e->getMessage());
        }
    }

    public function fetchCountByQuery($query, $params = [])
    {
        try {
            $statement = $this->db->prepare($query);
            $statement->execute($params);
            $result = $statement->fetch(PDO::FETCH_ASSOC);   
            return (int) $result['total'] ?? 0;
        } catch (\PDOException $e) {
            
            echo "Error fetching comment count: " . $e->getMessage();
            return 0;
        }
    }
    

   
}
