<?php

namespace App\Models;

use App\Models\Model;
use PDO;

class User extends Model
{ 
    protected $table = 'users';

    protected $id;
    protected $username;
    protected $password;
    protected $admin;

   
    private $conn;

    public function __construct(PDO $connection)
    {
        $this->conn = $connection;
    }
  
    public function getId(): ?int
    {
        return $this->id;
    }

   
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function isAdmin(): bool
    {
        return (bool)$this->admin;
    }

    public function setAdmin(bool $isAdmin): void
    {
        $this->admin = $isAdmin ? 1 : 0;
    }

    public function getUserByUsername($username)
    {
            $query = "SELECT * FROM {$this->table} WHERE username = :username";
            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(':username', $username, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            if ($row !== false) {
                $user = new User($this->conn);
                $user->setId($row['id']);
                $user->setUsername($row['username']);
                $user->setPassword($row['password']);
                $user->setAdmin((bool)$row['admin']);
                return $user;
            } else {
                return null;
            }
    }
    public function isClientLoggedIn()
    {        
        return isset($_SESSION['client_id']);
    }

    public function logoutClient() {
        unset($_SESSION['client_id']);
    }



   
}
