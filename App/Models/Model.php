<?php

namespace App\Models;

use PDO;

class Model {
    protected $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

   

    // Méthode pour exécuter une requête SQL
    protected function executeQuery($sql, $params = []) {
        $statement = $this->db->prepare($sql);
        $statement->execute($params);
        return $statement;
    }

    // Méthode pour exécuter une requête SQL et récupérer tous les résultats
    protected function fetchAll($sql, $params = []) {     
        $stmt = $this->db->prepare($sql);     
        $stmt->execute($params);
        $preparedSql = $stmt->queryString;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function fetchAllByQuery($query, $params = [])
    {
        try {
            $stmt = $this->db->prepare($query);
    
            foreach ($params as $key => $value) {
              
                if ($key === 'limit' || $key === 'offset') {
                    $stmt->bindValue(':' . $key, (int)$value, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue(':' . $key, $value);
                }
            }
    
            $stmt->execute();
    
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            // Handle the exception (e.g., log or display an error message)
            throw $e;
        }
    }

    // Méthode pour exécuter une requête SQL et récupérer un seul résultat
    protected function fetchSingle($sql, $params = []) {
        $statement = $this->executeQuery($sql, $params);
      return $statement->fetch(PDO::FETCH_ASSOC);

    }

// Méthode pour insérer des données dans la base de données
protected function insert($table, $data) {
    $columns = implode(', ', array_keys($data));
    $placeholders = ':' . implode(', :', array_keys($data));
    $sql = "INSERT INTO $table ($columns) VALUES ($placeholders)";
    $this->executeQuery($sql, $data);
}
protected function findById($table, $id) {
    $sql = "SELECT * FROM $table WHERE id = :id";
    $params = ['id' => $id];
    $statement = $this->executeQuery($sql, $params);
    return $statement->fetch(PDO::FETCH_ASSOC);
}
// Méthode pour mettre à jour des données dans la base de données
protected function update($table, $data, $where) {
    $setValues = '';
    foreach ($data as $column => $value) {
        $setValues .= "$column = :$column, ";
    }
    $setValues = rtrim($setValues, ', ');
    $sql = "UPDATE $table SET $setValues WHERE $where";
    $this->executeQuery($sql, $data);
}

// Méthode pour supprimer des données de la base de données
protected function delete($table, $where, $params = []) {
    $sql = "DELETE FROM $table WHERE $where";
    $this->executeQuery($sql, $params);
}

}