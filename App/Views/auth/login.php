<div class="container mt-4">

<h2>Connexion</h2>
<? 
   session_start();     
    
    if (isset($_SESSION['error'])) {
        echo '<p>' . $_SESSION['error'] . '</p>';
        unset($_SESSION['error']); 
    }
 
    ?>   
    <form action="/auth/login" method="post">
        <div class="form-group">
            <label for="username ">username  :</label>
            <input type="text" class="form-control" name="username" required>
        </div>
        <div class="form-group">
            <label for="password">Mot de passe :</label>
            <input type="password" class="form-control" name="password" required>
        </div>
        <button type="submit" class="btn btn-primary">Se connecter</button>
    </form>
</div>