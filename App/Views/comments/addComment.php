  <? 
   session_start();
 
   session_start();
   // Vérifiez si l'utilisateur est connecté
   if (!isset($_SESSION['id'])) {
       $_SESSION['error'] = "Vous devez être connecté pour accéder à cette page.";
       header('Location: /auth/login'); 
       exit();
   }
   
    if (isset($_SESSION['message'])) {
        echo '<p>' . $_SESSION['message'] . '</p>';
        unset($_SESSION['message']); 
    }
    ?>
<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmationModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to add this comment?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="confirmAddComment">Confirm</button>
      </div>
    </div>
  </div>
</div>

<div class="container mt-4">
    <h1>Ajouter un commentaire</h1>    
    <form id="addComment" method="post" action="javascript:void(0)">       
        <input type="hidden" name="produitId" value="123">
        <input type="hidden" name="clientId" value="<?= $clientId ?>">
        <div class="form-group">
            <label for="contenu">Contenu :</label>
            <textarea class="form-control" name="contenu" required></textarea>
        </div>
        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#confirmationModal">Ajouter</button>
    </form>
    <div id="successMessage" class="alert alert-success" style="display: none;"></div>
<br>

<a href="" >Déconnexion</a>
</div>


