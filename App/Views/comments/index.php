
    <div class="container">
        <div class="row">
            <div class="col">
                <h4>La sélection </h4>
                <img src="https://blissim.fr/wp-content/uploads/2021/11/cheerz_desktop-2.jpg" alt="Product Image" class="img-thumbnail" width="200">
                <p class="mt-3">Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisi.</p>
                <a href="/auth/login" class="btn btn-primary">Ajouter un commentaire</a>
            </div>
        </div>
       <hr />
    <h2>Comments</h2>
    <ul class="list-group">
        <?php foreach ($comments as $comment): ?>
            <li class="list-group-item">
                <strong>User <?php echo $comment['client_id']; ?>:</strong>
                <p><?php echo $comment['contenu']; ?>.</p>
                <small><?php echo $comment['date']; ?></small>
            </li>
        <?php endforeach; ?>
    </ul>

    <!-- Pagination links -->
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <?php             
            if ($paginationData['currentPage'] > 1): ?>
                <li class="page-item">
                    <a class="page-link" href="/comments/index/<?php echo $paginationData['currentPage'] - 1; ?>">Previous</a>
                </li>
            <?php endif; ?>

            <?php for ($i = 1; $i <= $paginationData['totalPages']; $i++): ?>
                <li class="page-item <?php echo $paginationData['currentPage'] === $i ? 'active' : ''; ?>">
                    <a class="page-link" href="/comments/index/<?php echo $i; ?>"><?php echo $i; ?></a>
                </li>
            <?php endfor; ?>

            <?php if ($paginationData['currentPage'] < $paginationData['totalPages']): ?>
                <li class="page-item">
                    <a class="page-link" href="/comments/index/<?php echo $paginationData['currentPage'] + 1; ?>">Next</a>
                </li>
            <?php endif; ?>
        </ul>
    </nav>
</div>


 