<?php
namespace App\Testes;

use App\App;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\ServerRequest;

class AppTest extends TestCase
{
    public function testRederectTrailingSlash()
    {
        $app = new App();
       $request = new ServerRequest('Get','/azerty/');
       $response= $app->run($request);
       $this->assertContains('/azerty/',$response->getHeader('Location'));
       $this->assertEquals(301,$response->getStatusCode());


    }
}
