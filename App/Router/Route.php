<?php

class Route
{
    private $routes = [];

    // Ajoute une route à la liste des routes
    public function addRoute($url, $controllerName, $actionName)
    {
        $this->routes[$url] = [
            'controller' => $controllerName,
            'action' => $actionName,
        ];
    }

    // Trouve et exécute la route correspondante
    public function executeRoute($url)
    {
        if (array_key_exists($url, $this->routes)) {
            $controllerName = $this->routes[$url]['controller'];
            $actionName = $this->routes[$url]['action'];

            // Charge le fichier du contrôleur
            require_once 'controllers/' . $controllerName . '.php';

            // Instancie le contrôleur et exécute l'action
            $controller = new $controllerName();
            $controller->$actionName();
        } else {
            // Gère l'URL invalide ici (par exemple, affiche une page d'erreur 404)
            echo 'Page non trouvée (erreur 404)';
        }
    }
}
