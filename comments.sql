Projet de Fonctionnalité de Commentaires
Description
Ce projet est une implémentation d une fonctionnalité de commentaires pour un site de commerce électronique permettant aux clients de laisser des commentaires sur les produits. Les commentaires sont stockés dans une base de données MySQL et paginés pour faciliter la navigation.

Fonctionnalités
Les clients peuvent se connecter pour laisser un commentaire sur un produit.
Les commentaires sont affichés sur la page du produit correspondant.
Les commentaires sont paginés pour faciliter la navigation.
La fonctionnalité est sécurisée contre les attaques XSS et les injections SQL.
Technologies Utilisées
PHP
MySQL
HTML
CSS
JavaScript

Installation
Clonez ce repository sur votre machine locale en utilisant la commande suivante :
bash
Copy code
git clone https://gitlab.com/hadjs/productfeedbackportal
Configurez votre environnement PHP et assurez-vous d avoir accès à une base de données MySQL.

Importez le fichier SQL "comments.sql" dans votre base de données pour créer la table "comments" et pré-remplir quelques données de test.

Assurez-vous d'avoir les dépendances requises en installant les packages Composer :

bash
Copy code
composer install
Configurez les paramètres de connexion à la base de données en modifiant le fichier "config.php" situé dans le dossier "config".

Lancez le serveur PHP en utilisant la commande :

bash
Copy code
php -S localhost:8000
Accédez à l'application en ouvrant votre navigateur et en visitant l'URL suivante :
arduino
Copy code
http://localhost:8000/comments/index/1

Fonctionnement
Sur la page d'accueil, les clients peuvent parcourir les produits disponibles.

Cliquez sur un produit pour accéder à sa page détaillée.

Sur la page du produit, les commentaires des clients seront affichés et paginés.

Les clients peuvent se connecter en utilisant leur identifiant et leur mot de passe pour laisser un commentaire.

Auteur
[hadj houda]

