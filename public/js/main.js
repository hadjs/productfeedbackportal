function handleAddCommentFormSubmission() {
  $('#confirmationModal').modal('hide');   
  $.ajax({
    url: '/comments/addComment', 
    type: 'POST',
    data: $('#addComment').serialize(),
    success: function (response) {
      const currentPage = 1; 
    //  const currentPath = window.location.pathname;
      const redirectUrl = `/comments/index/${currentPage}`;
      window.location.href = redirectUrl;
    },
    error: function (xhr, status, error) {
      // Handle the error if needed
      console.log(xhr.responseText);
    },
  });
}

document.addEventListener('DOMContentLoaded', function() {
  // Add event listener to the "Ajouter" button to show the modal
  document.getElementById('confirmAddComment').addEventListener('click', function() {
    handleAddCommentFormSubmission();
  });
});
