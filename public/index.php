<?php

require '../vendor/autoload.php';

use FastRoute\RouteCollector;
use FastRoute\Dispatcher;
use App\Controllers\CommentsController;

// Définition des routes
$dispatcher = FastRoute\simpleDispatcher(function (RouteCollector $r) {    
 //   $r->addRoute('GET', '/', 'CommentsController@index');   
    $r->addRoute('GET', '/comments/index/{page:\d+}', 'CommentsController@index');
    $r->addRoute('GET', '/comments/{produitId:\d+}', 'CommentsController@showCommentsByproduitId');
    $r->addRoute(['GET', 'POST'], '/comments/addComment', 'CommentsController@addComment');
    $r->addRoute(['GET', 'POST'], '/auth/login', 'UserController@login');
    $r->addRoute(['GET', 'POST'], '/auth/logout', 'UserController@logout');

});

// Récupération des détails de la requête
$httpMethod = $_SERVER['REQUEST_METHOD'];

$uri = $_SERVER['REQUEST_URI'];

// Suppression des éventuels paramètres GET
if (($pos = strpos($uri, '?')) !== false) {
    $uri = substr($uri, 0, $pos);
}

// Dispatch des routes
$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

// Traitement des résultats de dispatch
switch ($routeInfo[0]) {
    case Dispatcher::NOT_FOUND:
        // Route non trouvée - Afficher une page d'erreur 404
        echo 'Page non trouvée (erreur 404)';
        break;
    case Dispatcher::METHOD_NOT_ALLOWED:
        // Méthode HTTP non autorisée - Afficher une page d'erreur 405
        echo 'Méthode non autorisée (erreur 405)';
        break;
    case Dispatcher::FOUND:
        // Route trouvée - Exécuter le gestionnaire de la route
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        // Découpe le contrôleur et l'action à partir de la chaîne handler
        [$controllerName, $actionName] = explode('@', $handler);

        // Instancie le contrôleur et exécute l'action en passant le tableau $vars
        $controllerClass = 'App\\Controllers\\' . $controllerName;
      
        $controller = new $controllerClass();   
            
        $controller->$actionName($vars); // Passer le tableau $vars à la méthode
        break;
    }